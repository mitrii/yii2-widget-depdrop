<?php

namespace mitrii\widgets;

class DepDropExtAsset extends \yii\web\AssetBundle
{
    /**
     * Set up CSS and JS asset arrays based on the base-file names
     *
     * @param string $type whether 'css' or 'js'
     * @param array  $files the list of 'css' or 'js' basefile names
     */
    protected function setupAssets($type, $files = [])
    {

        $srcFiles = [];
        $minFiles = [];
        foreach ($files as $file) {
            $srcFiles[] = "{$file}.{$type}";
            $minFiles[] = "{$file}.min.{$type}";
        }
        $this->$type = YII_DEBUG ? $srcFiles : $minFiles;
    }

    /**
     * Sets the source path if empty
     *
     * @param string $path the path to be set
     */
    protected function setSourcePath($path)
    {
        $this->sourcePath = $path;
    }

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/depdrop']);
        parent::init();
    }

}
