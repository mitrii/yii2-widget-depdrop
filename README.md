Yii2 depdrop widget
===================
Yii2 dependent dropdown widget

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist mitrii/yii2-widget-depdrop "*"
```

or add

```
"mitrii/yii2-widget-depdrop": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \mitrii\depdrop\DepDrop::widget(); ?>```