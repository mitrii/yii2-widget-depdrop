<?php

/**
 * @copyright Copyright &copy; mitrii, original author Kartik Visweswaran, Krajee.com, 2014 - 2015
 * @package yii2-widgets
 * @subpackage yii2-widget-depdrop
 * @version 1.0.1
 */

namespace mitrii\widgets;


class DepDropAsset extends \yii\web\AssetBundle
{
    /**
     * Set up CSS and JS asset arrays based on the base-file names
     *
     * @param string $type whether 'css' or 'js'
     * @param array  $files the list of 'css' or 'js' basefile names
     */
    protected function setupAssets($type, $files = [])
    {

        $srcFiles = [];
        $minFiles = [];
        foreach ($files as $file) {
            $srcFiles[] = "{$file}.{$type}";
            $minFiles[] = "{$file}.min.{$type}";
        }
        $this->$type = YII_DEBUG ? $srcFiles : $minFiles;
    }

    /**
     * Sets the source path if empty
     *
     * @param string $path the path to be set
     */
    protected function setSourcePath($path)
    {
        $this->sourcePath = $path;
    }

    public function init()
    {
        $this->setSourcePath('@vendor/kartik-v/dependent-dropdown');
        $this->setupAssets('css', ['css/dependent-dropdown']);
        $this->setupAssets('js', ['js/dependent-dropdown']);

        parent::init();
    }
}
